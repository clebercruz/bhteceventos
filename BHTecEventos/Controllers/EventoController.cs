﻿using BHTecEventos.AuxCode;
using BHTecEventos.Controllers;
using BHTecEventos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace BHTecEventos.Controllers
{
    public class EventoController : Controller
    {
        private EntitiesBHTecEventos db = new EntitiesBHTecEventos();

        //
        // GET: /Evento/

        public ActionResult Index()
        {
            return View(new EventoCPFModel());

        }

        [HttpGet]
        public ActionResult CadastrarEvento()
        {
            return View();
        }


        //POST: /Evento/CadastrarEvento
        //[CustomAuthorize(new int[] { 3 })]
        [HttpPost]
        public ActionResult CadastrarEvento(CadastroEventoModel m)
        {
            TempData["atencao"] = "";
            TempData["mensagem"] = "";

            if (ModelState.IsValid)
            {
                this.AdicionarEvento(m);
                TempData["mensagem"] = "Evento criado com sucesso.";
                
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
                            .SelectMany(v => v.Errors)
                            .Select(e => e.ErrorMessage));
                TempData["atencao"] = message;
                return View();
            }
            var model = db.Eventos.ToList();
            return View("IndexEvento", model);
        }

        public void AdicionarEvento(CadastroEventoModel cs)
        {

            var idUser = db.Usuarios.FirstOrDefault(a => a.Nome == User.Identity.Name);
            db.Eventos.Add(new Eventos
            {
                Id = cs.Id,
                Nome = cs.Nome,
                DataCriado = DateTime.Now,
                DataLimite = cs.DataLimite,
                DataAcontecimento = cs.DataAcontecimento,
                UsuarioResponsavel = Convert.ToInt32(idUser.Codigo),
                LimitePessoas = cs.LimitePessoas,
                CargaHoraria = cs.CargaHoraria,
                Organizador = cs.Organizador,
                Ativo = cs.Ativo

            });
            db.SaveChanges();

        }


        //[CustomAuthorize(new int[] { 3 })]
        public ActionResult IndexEvento()
        {

            var model = db.Eventos.ToList();
            return View(model);
        }

        //[CustomAuthorize(new int[] { 3 })]
        public ActionResult EditEvento(int id = 0)
        {
            var model = db.Eventos.Find(id);
            EventoModel SM = new EventoModel();
            SM.Id = model.Id;
            SM.Nome = model.Nome;
            SM.DataCriado = model.DataCriado;
            SM.DataLimite = model.DataLimite.Date.ToString();
            SM.DataAcontecimento = model.DataAcontecimento.Date.ToString();
            SM.LimitePessoas = model.LimitePessoas;
            SM.CargaHoraria = model.CargaHoraria;
            SM.Organizador = model.Organizador;
            SM.UsuarioResponsavel = model.UsuarioResponsavel;
            SM.NomeUsuario = model.Usuarios.Nome;
            SM.Ativo = model.Ativo;


            return View(SM);
        }

        //[CustomAuthorize(new int[] { 3 })]
        [HttpPost]
        public ActionResult EditEvento(Eventos model)
        {
            var idUser = db.Usuarios.FirstOrDefault(a => a.Nome == User.Identity.Name);

            TempData["mensagem"] = "";

            if (ModelState.IsValid)
            {
                var SM = db.Eventos.Find(model.Id);

                SM.Nome = model.Nome;
                SM.DataLimite = model.DataLimite;
                SM.DataAcontecimento = model.DataAcontecimento;
                SM.LimitePessoas = model.LimitePessoas;
                SM.CargaHoraria = model.CargaHoraria;
                SM.Organizador = model.Organizador;
                SM.UsuarioResponsavel = Convert.ToInt32(idUser.Codigo);
                SM.Ativo = model.Ativo;

                db.SaveChanges();
                TempData["mensagem"] = "Evento alterado com sucesso";
                return RedirectToAction("IndexEvento");
            }
            else
            {
                return View();
            }


        }

        public ActionResult DeleteEvento(int id = 0)
        {
            TempData["mensagem"] = "";
            var Evento = db.Eventos.Find(id);
            db.Eventos.Remove(Evento);
            db.SaveChanges();
            TempData["mensagem"] = "Seminário excluído com sucesso.";
            return RedirectToAction("IndexEvento");
        }



        public ActionResult BuscaCadastrados()
        {
            return View("~/Views/Evento/BuscaCadastrados.cshtml");
        }

        public ActionResult BuscaCadastroPessoa(EventoCPFModel Candidato)
        {

            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            if (ModelState.IsValid)
            {
                //Verificar se é o administrador do sistema e redirecionar para tela de login
                //var Usu = db.Usuarios.FirstOrDefault(c => c.CPF == Candidato.CPF);
                //if (Usu != null)
                //{
                //    return View("~/Views/Admin/Login.cshtml");
                //}
                PessoaModel P = new PessoaModel();
                var Pessoa = db.Pessoas.FirstOrDefault(u => u.CPF == Candidato.CPF);
                if (Pessoa != null)
                {
                    return View("AtualizarPessoas", P.BuscarPessoa(Pessoa));
                }
                else
                {
                    return View("CadastrarPessoas");
                }
            }
            else
            {
                return View("Index");
            }
        }


        public ActionResult RegistraCandidatoAoEvento(String CPF, int Evento)
        {
            string Erro = "";
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            if (ModelState.IsValid)
            {

                var EstorouLimite = db.Eventos.Find(Evento);
                if (db.Pessoas_Eventos.Where(i => i.IdEvento == Evento).Count() >= EstorouLimite.LimitePessoas)
                {
                    TempData["atencao"] = "Não há mais vagas disponíveis para este evento. Obrigado";
                    return View("~/Views/Evento/MensagemSucessoPessoaEvento.cshtml");
                }

                var DataLimite = db.Eventos.Find(Evento).DataLimite;
                if (System.DateTime.Now > DataLimite)
                {
                    TempData["atencao"] = "O prazo para inscrição terminou no dia " + DataLimite.ToString("dd/MM/yyyy") + ". Obrigado";
                    return View("~/Views/Evento/MensagemSucessoPessoaEvento.cshtml");
                }
                var Pessoa = db.Pessoas.FirstOrDefault(n => n.CPF == CPF);

                if (VerificaSePessoaEstaNoEvento(Pessoa.Id, Convert.ToInt32(Evento)))
                {
                    TempData["mensagem"] = "Você já está cadastrado para o evento solicitado. Obrigado";
                    return View("~/Views/Evento/MensagemSucessoPessoaEvento.cshtml");
                }

                if (Pessoa != null)
                {
                    try
                    {
                        Pessoas_Eventos Gravar = new Pessoas_Eventos();
                        Gravar.IdPessoa = Pessoa.Id;
                        Gravar.IdEvento = Evento;
                        Gravar.DataInscricao = System.DateTime.Now;
                        db.Pessoas_Eventos.Add(Gravar);
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            //Erro += "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:" + eve.Entry.Entity.GetType().Name + eve.Entry.State;
                            foreach (var ve in eve.ValidationErrors)
                            {
                                //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",ve.PropertyName, ve.ErrorMessage);
                                //Erro += "- Property: \"{0}\", Error: \"{1}\"" + ve.PropertyName + ve.ErrorMessage;
                                Erro += ve.ErrorMessage + "\n";
                            }
                        }
                        TempData["atencao"] = Erro;
                        return View("~/Views/Evento/CadastrarPessoaNoEvento.cshtml");
                    }
                }


            }
            TempData["mensagem"] = "Cadastro Realizado com sucesso. Obrigado.";
            return View("~/Views/Evento/MensagemSucessoPessoaEvento.cshtml");
        }

        private bool VerificaSePessoaEstaNoEvento(int Pessoa, int Evet)
        {
            var Evento = db.Pessoas_Eventos.FirstOrDefault(n => n.IdPessoa == Pessoa && n.IdEvento == Evet);
            if (Evento != null) return true;
            else { return false; }
        }

        public ActionResult Busca(String Nome, String Evento, String DataInicio, String DataFim)
        {
            //AJAX
            var query = db.ListaPessoasDoEvento.AsQueryable();
            if (ModelState.IsValid)
            {
                if (!String.IsNullOrEmpty(Nome))
                {
                    //var resultado = db.ListaPessoasDoEvento.Where(u => u.Nome.Contains(Nome)).OrderBy(u => u.Nome).ToList();
                    //return PartialView("~/Views/Evento/ListaPessoasCadastradas.cshtml", resultado);
                    query = query.Where(u => u.Nome.Contains(Nome)).OrderBy(u => u.Nome);
                }
                if (!String.IsNullOrEmpty(Evento))
                {
                    int EventoId = Convert.ToInt32(Evento);
                    //var resultado = db.ListaPessoasDoEvento.Where(u => u.IdEvento == EventoId).ToList();
                    //return PartialView("~/Views/Evento/ListaPessoasCadastradas.cshtml", resultado);
                    query = query.Where(u => u.IdEvento == EventoId);
                }
                if (DataInicio != "" && DataFim != "")
                {
                    var dataI = Convert.ToDateTime(DataInicio);
                    var dataF = Convert.ToDateTime(DataFim);
                    //var resultado = db.ListaPessoasDoEvento.Where(u => u.DataInscricao >= dataI && u.DataInscricao <= dataF).ToList();
                    //return PartialView("ListaPessoasCadastradas", resultado);
                    query = query.Where(u => u.DataInscricao >= dataI && u.DataInscricao <= dataF);
                }
                else if (DataInicio != "")
                {
                    var dataI = Convert.ToDateTime(DataInicio);
                    //var resultado = db.ListaPessoasDoEvento.Where(u => u.DataInscricao >= dataI).ToList();
                    //return PartialView("ListaPessoasCadastradas", resultado);
                    query = query.Where(u => u.DataInscricao >= dataI);
                }
                else if (DataFim != "")
                {
                    var dataF = Convert.ToDateTime(DataFim);
                    //var resultado = db.ListaPessoasDoEvento.Where(u => u.DataInscricao <= dataF).ToList();
                    //return PartialView("ListaPessoasCadastradas", resultado);
                    query = query.Where(u => u.DataInscricao <= dataF);
                }
            }
            var resultado = query.ToList();
            return PartialView("ListaPessoasCadastradas", resultado);
        }

        public ActionResult EmitirCertificado()
        {
            return View("Certificado");
        }

        [HttpPost]
        public ActionResult AlterarStatus(Pessoas_Eventos model)
        {
            var pessoaEvento = db.Pessoas_Eventos.Find(model.IdEvento, model.IdPessoa);

            var resultado = new List<Pessoas_Eventos>();

            pessoaEvento.Presenca = true;

            model.Presenca = pessoaEvento.Presenca;

            db.SaveChanges();
            return PartialView("ListaPessoasCadastradas", resultado);

        }

        [HttpGet]
        public JsonResult AlterarStatus(string id)
        {
            int pos = id.IndexOf("|");

            int IdPessoa = Convert.ToInt32(id.Substring(0, pos));
            int IdEvento = Convert.ToInt32(id.Substring(pos + 1));

            var pessoaEvento = db.Pessoas_Eventos.Find(IdPessoa, IdEvento);
            if (pessoaEvento.Presenca == false)
            {
                pessoaEvento.Presenca = true;
                db.SaveChanges();
                return Json(new { pres = pessoaEvento.Presenca, nome = pessoaEvento.Pessoas.Nome, tipo = "0" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { pres = pessoaEvento.Presenca, nome = pessoaEvento.Pessoas.Nome, tipo = "1" }, JsonRequestBehavior.AllowGet); ;
            }
        }

        public ActionResult VisualizarPessoa(int idPes = 0, int idSemi = 0)
        {
            var pessoa = db.Pessoas.Find(idPes);
            var Semi = db.Eventos.Find(idSemi);
            if (pessoa != null)
            {
                ExibePessoaModel model = new ExibePessoaModel();
                model.QntEvento = db.Pessoas_Eventos.Where(u => u.IdPessoa == pessoa.Id && u.IdEvento == Semi.Id).ToList();
                model.Pessoa = pessoa;
                model.Evento = Semi;
                return View("~/Views/Evento/VisualizarPessoa.cshtml", model);
            }
            else
            {
                return View("~/Views/Evento/VisualizarPessoa.cshtml");
            }

        }


        [HttpGet]
        public ActionResult ComprovanteEvento(int idPes, int idSmr)
        {
            var ps = db.Pessoas_Eventos.FirstOrDefault(u => u.IdPessoa == idPes && u.IdEvento == idSmr);
            if (ps != null && (ps.Presenca))
            {
                Documento.geraComprovanteBHTecEventos(ps);
                return View();
            }
            else
            {
                return RedirectToAction("BuscaCadastrados");
            }
        }



        [HttpGet]
        public ActionResult Relatorio()
        {
            TempData["atencao"] = "";
            TempData["mensagem"] = "";

            if (db.Eventos.Count() > 0)
            {
                RelEventoModel m = new RelEventoModel();
                m.Postos = new List<PostoEventoModel>();


                var pessoas = db.Pessoas_Eventos.ToList();

                m.TotalIncritos = pessoas.Count;
                m.TotalPresente = pessoas.Where(p => p.Presenca == true).Count();
                m.NomeEvento = db.Eventos.FirstOrDefault(s => s.Ativo == true).Nome;

                List<int> listaEventos = pessoas.Select(p => p.IdEvento).Distinct().ToList();

                foreach (var item in listaEventos)
                {
                    var quantidadeIncrito = pessoas.Where(p => p.IdEvento == item).Count();
                    var quantidadePresentes = pessoas.Where(p => p.IdEvento == item && p.Presenca == true).Count();

                    var DescEvento = db.Eventos.Find(item);

                    m.Postos.Add(new PostoEventoModel
                    {
                        Cod = item,
                        Quant = quantidadeIncrito,
                        Descricao = DescEvento.Nome,
                        QuantPresentes = quantidadePresentes
                    });
                }

                return View(m);
            }
            TempData["atencao"] = "Não existem eventos cadastrados";
            return View();
        }


    }
}
