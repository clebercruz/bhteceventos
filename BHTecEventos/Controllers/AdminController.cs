﻿using BHTecEventos.AuxCode;
using BHTecEventos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BHTecEventos.Controllers
{
    public class AdminController : Controller
    {
        private EntitiesBHTecEventos db = new EntitiesBHTecEventos();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(AdminLoginModel model)
        {
            bool dados = false, ativo = false;
            if (ModelState.IsValid)
            {
                if (Sec.IsValidAdmin(TT.SemAcentoMaiuscula(model.CPFUsuario), model.Senha))
                {
                    if (Sec.IsActive(TT.SemAcentoMaiuscula(model.CPFUsuario), model.Senha))
                    {
                        FormsAuthentication.SetAuthCookie(db.Usuarios.FirstOrDefault(c => c.CPF == model.CPFUsuario).Nome.ToString(), false);
                        return RedirectToAction("BuscaCadastrados", "Evento");
                    }
                    else
                    {
                        ativo = true;
                    }
                }
                else
                {
                    dados = true;
                }
            }
            //Este erro aparece em @Html.ValidationSummary(true, null, new { @style = "color: #f00" });
            if (dados)
                ModelState.AddModelError("", "Dados incorretos.");
            if (ativo)
                ModelState.AddModelError("", "Funcionário bloqueado.");

            return View(model);
        }

        [Authorize]
        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Admin");
        }

        public ActionResult RecuperaSenhaUsuario()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RecuperaSenhaUsuario(String CPF)
        {
            //Criando DataContext.
            var db = new EntitiesBHTecEventos();

            //Pegando um usuário no banco.
            var Usuario = db.Usuarios.FirstOrDefault(u => u.CPF == CPF);

            //Caso exista.
            if (Usuario != null)
            {
                //Enviando um email.
                Email em = new Email(Usuario.Email, "Recuperação de senha INOVA-UFMG", "Olá " + Usuario.Nome + ", sua senha é: " + Sec.Decrypt(Usuario.Senha) + ".");
                em.Envia();

                //Esta é a mensagem que será mostrada.
                ViewData["mensagem"] = "Email enviado com sucesso.";

                return View();
            }
            //Caso não exista.
            else
            {
                ViewData["mensagem"] = "Este não é um CPF válido.";
                return View();
            }
        }


    }
}