﻿using BHTecEventos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BHTecEventos.AuxCode;
using System.Web.Security;

namespace BHTecEventos.Controllers
{
    public class UsuariosController : Controller
    {
        private EntitiesBHTecEventos db = new EntitiesBHTecEventos();

        [HttpPost]
        public ActionResult Login(EventoCPFModel Candidato)
        {
            if (Request.IsAuthenticated) FormsAuthentication.SignOut();
            return RedirectToAction("BuscaCadastroPessoa","Evento",Candidato);
        }

        public ActionResult ListaUsuarios()
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            return View(db.Usuarios.ToList());
        }


        public ActionResult CadastrarUsuario()
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            return View();
        }

        [HttpPost]
        public ActionResult CadastrarUsuario(Usuarios U)
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            if (ModelState.IsValid)
            {
                try
                {
                    U.Senha = Sec.Encrypt(U.Senha);
                    db.Usuarios.Add(U);
                    db.SaveChanges();
                    TempData["mensagem"] = "Usuário Cadastrado com sucesso.";
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        //Erro += "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:" + eve.Entry.Entity.GetType().Name + eve.Entry.State;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",ve.PropertyName, ve.ErrorMessage);
                            //Erro += "- Property: \"{0}\", Error: \"{1}\"" + ve.PropertyName + ve.ErrorMessage;
                            TempData["atencao"] += ve.ErrorMessage + "\n";
                        }
                    }

                }
            }
            return View(U);
        }

        public ActionResult AtualizarUsuario(int id)
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            return View(db.Usuarios.Find(id));
        }

        [HttpPost]
        public ActionResult AtualizarUsuario(Usuarios U)
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            if (ModelState.IsValid)
            {

                try
                {
                    Usuarios LU = db.Usuarios.Find(U.Codigo);
                    LU.Nome = U.Nome;
                    LU.Email = U.Email;
                    LU.CPF = U.CPF;
                    LU.Telefone = U.Telefone;
                    LU.Ativo = U.Ativo;
                    LU.Senha = Sec.Encrypt(U.Senha); ;
                    db.Entry(LU).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["mensagem"] = "Usuário Cadastrado com sucesso.";
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        //Erro += "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:" + eve.Entry.Entity.GetType().Name + eve.Entry.State;
                        foreach (var ve in eve.ValidationErrors)
                        {
                            //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",ve.PropertyName, ve.ErrorMessage);
                            //Erro += "- Property: \"{0}\", Error: \"{1}\"" + ve.PropertyName + ve.ErrorMessage;
                            TempData["atencao"] += ve.ErrorMessage + "\n";
                        }
                    }

                }
            }
            return View(U);
        }

        public ActionResult ExcluirUsuario(int id)
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            Usuarios U = db.Usuarios.Find(id);
            if (U.Nome == "Administrador")
            {
                TempData["atencao"] = "Este usário não pode ser Excluído";
            }
            else
            {
                db.Usuarios.Remove(U);
                db.SaveChanges();
            }
            return View("ListaUsuarios", db.Usuarios.ToList());
        }


    }
}
