﻿using BHTecEventos.Controllers;
using BHTecEventos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BHTecEventos.Controllers
{
    public class PessoaController : Controller
    {
        //
        // GET: /Pessoa/

        private EntitiesBHTecEventos db = new EntitiesBHTecEventos();

        //
        // GET: /Evento/

        public ActionResult Index()
        {
            return View(new EventoCPFModel());
        }

        public ActionResult CadastrarPessoas()
        {
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            return View("~/Views/Evento/CadastrarPessoas.cshtml");
        }

        [HttpPost]
        public ActionResult CadastrarPessoas(PessoaModel Pessoa)
        {
            string Erro = "";
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            if (ModelState.IsValid)
            {
                Erro = Pessoa.CadastrarPessoa(Pessoa);
                if (Erro == "") //Sucesso na gravação
                {
                    TempData["mensagem"] = "Cadastro Realizado com sucesso. Para concluir escolha o evento que deseja Participar";
                    Pessoa = new PessoaModel().BuscarPessoa(db.Pessoas.FirstOrDefault(c => c.CPF == Pessoa.CPF));
                    return View("~/Views/Evento/CadastrarPessoaNoEvento.cshtml", Pessoa);
                }
                else
                {
                    TempData["atencao"] = Erro;
                    return View("~/Views/Evento/CadastrarPessoas.cshtml");
                }
            }
            return View("~/Views/Evento/CadastrarPessoas.cshtml");
        }

        [HttpPost]
        public ActionResult AtualizarPessoas(PessoaModel Pessoa)
        {
            string Erro = "";
            TempData["mensagem"] = "";
            TempData["atencao"] = "";
            if (ModelState.IsValid)
            {
                Erro = Pessoa.AtualizarPessoa(Pessoa);
                if (Erro == "") //Sucesso na gravação
                {
                    TempData["mensagem"] = "Cadastro Realizado com sucesso. Para concluir escolha o evento que deseja Participar";
                    Pessoa = new PessoaModel().BuscarPessoa(db.Pessoas.FirstOrDefault(c => c.CPF == Pessoa.CPF));
                    return View("~/Views/Evento/CadastrarPessoaNoEvento.cshtml", Pessoa);
                }
                else
                {
                    TempData["atencao"] = Erro;
                    return View("~/Views/Evento/AtualizarPessoas.cshtml");
                }
            }
            return View("~/Views/Evento/AtualizarPessoas.cshtml");

        }

    }
}
