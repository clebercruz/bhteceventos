﻿jQuery(function ($) {
    $(document).ready(function () {
            ContarCaracteres();
            criaOnClickProxAnt();

            $("#CPF").mask("999.999.999-99");
            $('#Telefone').mask("(99) 9999-9999?9");
            $('#Telefone').change(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                phone = target.value.replace(/\D/g, '');
                element = $(target); element.unmask();
                if (phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                }
                else {
                    element.mask("(99) 9999-9999?9");
                }
            });
            $("#CEP").mask("99999-999");
            $("#DataNasc").mask("99/99/9999");
            $("#InicioMedidaSocioEducativa").mask("99/99/9999");
            $("#FimMedidaSocioEducativa").mask("99/99/9999");
            $(".valor_num").maskMoney({ thousands: '', decimal: ',', affixesStay: false });

            $(".maxlength").keyup(function () {
                ContarCaracteres();
            });

            $("#InseridoBolsaFamilia").change(function () {
                if ($("#InseridoBolsaFamilia").prop('checked')) {
                    $("#check_bolsafamilia").show('slow');
                }
                else {
                    $("#check_bolsafamilia").hide('slow');
                }

            });

            $("#InseridoPETI").change(function () {
                if ($("#InseridoPETI").prop('checked')) {
                    $("#check_peti").show('slow');
                }
                else {
                    $("#check_peti").hide('slow');
                }

            });

            $("#RealizouAtivAreaOutro").change(function () {
                if ($("#RealizouAtivAreaOutro").prop('checked')) {
                    $("#check_outraarea").show('slow');
                }
                else {
                    $("#check_outraarea").hide('slow');
                }

            });

            $("#TemBeneficioAssistencial").change(function () {
                var val = $('input[type="radio"][name="TemBeneficioAssistencial"]:checked').val();
                if (val) {
                    $("#check_benassistencial").show('slow');
                }
                else {
                    $("#check_benassistencial").hide('slow');
                }

            });

            $("#TipoCasa").change(function () {
                var pag = $(this).val();

                if (pag == '2') {
                    $("#check_vlafinanciamento").hide('slow');
                    $("#check_quemcede").hide('slow');
                    $("#check_vlaluguel").show('slow');
                }
                else if (pag == '6') {
                    $("#check_vlaluguel").hide('slow');
                    $("#check_quemcede").hide('slow');
                    $("#check_vlafinanciamento").show('slow');
                }
                else if (pag == '3') {
                    $("#check_vlaluguel").hide('slow');
                    $("#check_vlafinanciamento").hide('slow');
                    $("#check_quemcede").show('slow');
                }
                else {
                    $("#check_vlaluguel").hide('slow');
                    $("#check_vlafinanciamento").hide('slow');
                    $("#check_quemcede").hide('slow');
                }


            });
    });
});


//funções
function retira_acentos(palavra) {
    com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ';
    sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    nova = '';
    for (i = 0; i < palavra.length; i++) {
        if (com_acento.search(palavra.substr(i, 1)) >= 0) {
            nova += sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1);
        }
        else {
            nova += palavra.substr(i, 1);
        }
    }
    return nova.toUpperCase();
}

function getEndereco() {
    if ($.trim($("#CEP").val()) != "") {

        var cep_code = $("#CEP").val();
        if (cep_code.length <= 0) return;
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
           function (result) {
               if (result.status != 1) {
                   alert(result.message || "Houve um erro desconhecido");
                   return;
               }
               $("#Municipio").val(retira_acentos(unescape(result.city)));
               $("#Bairro").val(unescape(result.district));
               $("#Rua").val(unescape(result.address));
               $("#UF").val(unescape(result.state));
           });
    }
}

function ContarCaracteres() {
    var valor = 48;
    $(".maxlength").each(function () {
        valor -= $(this).val().length;
    })
    $("#caracter").html(valor + ' Caracteres.')
    if (valor < 0) {
        $('#botaoproximo2').hide();
        $("#caracter").css("color", "red");
    }
    else {
        $('#botaoproximo2').show();
        $("#caracter").css("color", "black");
    }
}

//Cria o evento Click para os botões Próximo/Anterior das seções
function criaOnClickProxAnt() {
    //$('#identificacao').hide();
    $('#endereco').hide();
    $('#situacaotrabalho').hide();
    $('#situacaohabtacional').hide();
    $('#saude').hide();
    $('#programaprojetoservico').hide();
    $('#medidaprotecao').hide();
    $('#providencia').hide();


    //BOTÕES PROXÍMOS
    $('#botaoproximo1').click(function () {
        var pt1 = $(".pt1")
        if (pt1.valid()) {
            $('#identificacao').hide('slow');
            $('#endereco').show('slow');
        }
    });

    $('#botaoproximo2').click(function () {
        $('#endereco').hide('slow');
        $('#situacaotrabalho').show('slow');
    });

    $('#botaoproximo3').click(function () {
        $('#situacaotrabalho').hide('slow');
        $('#situacaohabtacional').show('slow');
    });

    $('#botaoproximo4').click(function () {
        $('#situacaohabtacional').hide('slow');
        $('#saude').show('slow');
    });

    $('#botaoproximo5').click(function () {
        $('#saude').hide('slow');
        $('#programaprojetoservico').show('slow');
    });

    $('#botaoproximo6').click(function () {
        $('#programaprojetoservico').hide('slow');
        $('#medidaprotecao').show('slow');
    });

    $('#botaoproximo7').click(function () {
        $('#medidaprotecao').hide('slow');
        $('#providencia').show('slow');
    });

    //BOTÕES ANTERIORES
    $('#botaoanterior1').click(function () {
        $('#identificacao').show('slow');
        $('#endereco').hide('slow');
    });

    $('#botaoanterior2').click(function () {
        $('#endereco').show('slow');
        $('#situacaotrabalho').hide('slow');
    });

    $('#botaoanterior3').click(function () {
        $('#situacaotrabalho').show('slow');
        $('#situacaohabtacional').hide('slow');
    });

    $('#botaoanterior4').click(function () {
        $('#situacaohabtacional').show('slow');
        $('#saude').hide('slow');
    });

    $('#botaoanterior5').click(function () {
        $('#saude').show('slow');
        $('#programaprojetoservico').hide('slow');
    });

    $('#botaoanterior6').click(function () {
        $('#programaprojetoservico').show('slow');
        $('#medidaprotecao').hide('slow');
    });

    $('#botaoanterior7').click(function () {
        $('#medidaprotecao').show('slow');
        $('#providencia').hide('slow');
    });

}

function showValorBA() { $("#check_benassistencial").show('slow'); };
function hideValorBA() { $("#check_benassistencial").hide('slow'); };
function showDivsMedicamento() {
    $("#check_bulamedicamento").show('slow');
    $("#check_tempomedicamento").show('slow');
};
function hideDivsMedicamento() {
    $("#check_bulamedicamento").hide('slow');
    $("#check_tempomedicamento").hide('slow');
};
function showValorAtendPP() { $("#check_passouAtendPP").show('slow'); };
function hideValorAtendPP() { $("#check_passouAtendPP").hide('slow'); };
function showDivsSubsPsico() { $(".check_usaSubsPsico").show('slow'); };
function hideDivsSubsPsico() { $(".check_usaSubsPsico").hide('slow'); };
function showValorFanDefi() { $(".ckeck_familiatemdefi ").show('slow'); };
function hideValorFanDefi() { $(".ckeck_familiatemdefi ").hide('slow'); };
function showValorAlergiaP() { $("#check_alergia").show('slow'); };
function hideValorAlergia() { $("#check_alergia").hide('slow'); };
function showValorProtecao() { $("#check_protecao").show('slow'); };
function hideValorProtecao() { $("#check_protecao").hide('slow'); };
function showValorMediSocio() { $(".ckeck_mediSocio").show('slow'); };
function hideValorMediSocio() { $(".ckeck_mediSocio").hide('slow'); };

//showValorBA();
//hideValorBA();
//showDivsMedicamento();
//hideDivsMedicamento();
//showValorAtendPP();
//hideValorAtendPP();
//showDivsSubsPsico();
//hideDivsSubsPsico();
//showValorFanDefi();
//hideValorFanDefi();
//showValorAlergiaP();
//hideValorAlergia();
//showValorProtecao();
//hideValorProtecao();
//showValorMediSocio();
//hideValorMediSocio();