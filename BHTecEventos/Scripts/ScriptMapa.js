//Estes elementos precisam estar vis�veis em todos m�todos.
var map;
var geocoder;

var listaclubes = new Array();
var listaunidades = new Array();
var listahospedagem = new Array();
var listaeducacao = new Array();
var listajuridico = new Array();
var listadiversos = new Array();
var listaturismo = new Array();


//INFOWINDOW
var infoWindow = new google.maps.InfoWindow;
var onMarkerClick = function () {
    var marker = this;
    infoWindow.setContent(marker.descricao);
    infoWindow.open(map, marker);
};

function initialize() {
    //array de estilo.
    var styles = [
	 {
	     stylers: [
           { hue: "#465ea8" },
           { saturation: 0 }
	     ]
	 }, {
	     featureType: "road",
	     elementType: "geometry",
	     stylers: [
           { lightness: 70 },
           { visibility: "simplified" }
	     ]
	 }, {
	     //Define se os nomes das ruas aparecem.
	     featureType: "road",
	     elementType: "labels",
	     stylers: [
           { visibility: "on" }
	     ]
	 },
     {
         //Define se os pontos de onibus aparecem.
         featureType: "transit",
         elementType: "labels",
         stylers: [
           { visibility: "off" }
         ]
     },
     {
         //Define se os pontos de onibus aparecem.
         featureType: "poi",
         elementType: "labels",
         stylers: [
           { visibility: "off" }
         ]
     }
    ];

    // Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    var styledMap = new google.maps.StyledMapType(styles, { name: "Mapa" });

    // Create a map object, and include the MapTypeId to add
    // to the map type control.
    var mapOptions = {
        center: new google.maps.LatLng(-19.924741, -43.920827),
        zoom: 17,
        panControl: false,
        scrollwheel: false,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style']
        }
    };

    //Cria o mapa.
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    //busca de endere�o
    geocoder = new google.maps.Geocoder();
    //Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    function carregarPontos() {
        $.getJSON('/mapa/getPontos', function (pontos) {
            $.each(pontos, function (index, ponto) {
                //Icone do marcador.
                var image = 'images/point1.png';
                //Define o icone do marcador.
                switch (ponto.Tipo) {
                    case 1:
                        image = 'images/point1.png'; break;
                    case 2:
                        image = 'images/point2.png'; break;
                    case 3:
                        image = 'images/point3.png'; break;
                    case 4:
                        image = 'images/point4.png'; break;
                    case 5:
                        image = 'images/point5.png'; break;
                    case 6:
                        image = 'images/point6.png'; break;
                    case 7:
                        image = 'images/point7.png'; break;
                    case 8:
                        image = 'images/point8.png'; break;
                    default:
                        image = 'images/point.png';
                }
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(ponto.Latitude, ponto.Longitude),
                    //animacao que faz ele cair na tela
                    //animation: google.maps.Animation.DROP,
                    title: ponto.titulo,
                    //define o mapa.
                    map: map,
                    //Define o icone do marcador.
                    icon: image,
                });
                if (ponto.endereco = ! null) { var mais = "<button class='mod_botao' type='button' onclick='oi(\"" + ponto.titulo + "\")'>MAIS</button>" }
                //Propriedade dinamica para marcador.
                marker.descricao =
                    '<h2 id="firstHeading" class="firstHeading">' + ponto.titulo + '</h2>' + 
                    '<div id="bodyContent"><p>' + ponto.descricao + '</p></div>' + "<button class='mod_botao' type='button' onclick='oi(\""+ ponto.titulo +"\")'>MAIS</button>";

                //Define de qual lista o ponto faz parte.
                switch (ponto.Tipo) {
                    case 1:
                        listaunidades.push(marker); break;
                    case 2:
                        listaclubes.push(marker); break;
                    case 3:
                        listahospedagem.push(marker); break;
                    case 4:
                        listaeducacao.push(marker); break;
                    case 5:
                        listajuridico.push(marker); break;
                    case 6:
                        listadiversos.push(marker); break;
                    case 7:
                        listaturismo.push(marker); break;
                }

                //Evento que abre ums InfoWindow.
                google.maps.event.addListener(marker, 'click', onMarkerClick);
            });
        });
    }

    //Carrega os pontos.
    carregarPontos();

    //A InfoWindow fecha quando se clica no mapa.
    google.maps.event.addListener(map, 'click', function (){
        infoWindow.close();
    });
}

//Esconde os elementos do rodap�, exceto o inicial
$(document).ready(function () {
    initialize();
    function carregarNoMapa(endereco) {
        geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();

                    $('#txtEndereco').val(results[0].formatted_address);
                    $('#txtLatitude').val(latitude);
                    $('#txtLongitude').val(longitude);

                    var location = new google.maps.LatLng(latitude, longitude);
                    map.setCenter(location);
                    map.setZoom(14);
                }
            }
        });
    }

    $("#btnEndereco").click(function () {
        if ($(this).val() != "")
            carregarNoMapa($("#txtEndereco").val());
    })

    $("#txtEndereco").blur(function () {
        if ($(this).val() != "")
            carregarNoMapa($(this).val());
    })

    $("#txtEndereco").autocomplete({
        source: function (request, response) {
            geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
                response($.map(results, function (item) {
                    return {
                        label: item.formatted_address,
                        value: item.formatted_address,
                        latitude: item.geometry.location.lat(),
                        longitude: item.geometry.location.lng()
                    }
                }));
            })
        },
        select: function (event, ui) {
            $("#txtLatitude").val(ui.item.latitude);
            $("#txtLongitude").val(ui.item.longitude);
            var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
            marker.setPosition(location);
            map.setCenter(location);
            map.setZoom(16);
        }
    });

    $('#R_inicial').show();
    $('#R_regionais').hide();
    $('#R_juridico').hide();
    $('#R_lazer').hide();
    $('#R_hoteltransito').hide();
    $('#R_hotelpousada').hide();
    $('#R_educacao').hide();
    $('#R_diversos').hide();
});


    //Esconde o grupo desejado
    function hideGroup(group) {
        var length = group.length;
        var element = null;
        for (var i = 0; i < length; i++) {
            element = group[i];
            element.setVisible(false);
        }
    }

    //mostra o grupo desejado
    function showGroup(group) {
        var length = group.length;
        var element = null;
        for (var i = 0; i < length; i++) {
            element = group[i];
            element.setVisible(true);
        }
    }

    //Ajusta o zoom para mostrar os clubes.
    function showclubes() {
        infoWindow.close();

        hideGroup(listadiversos);
        hideGroup(listaturismo);
        hideGroup(listaunidades);
        hideGroup(listahospedagem);
        hideGroup(listaeducacao);
        hideGroup(listajuridico);
        showGroup(listaclubes);

        var markers = listaclubes;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        //Texto Rodap�
        $('#R_inicial').hide();
        $('#R_regionais').hide();
        $('#R_juridico').hide();
        $('#R_lazer').show();
        $('#R_hoteltransito').hide();
        $('#R_hotelpousada').hide();
        $('#R_educacao').hide();
        $('#R_diversos').hide();

        voltarcor();
        //cor de fundo
        $('#botaoclubes').css('background-color', '#ffc000');
        
    };

    //Ajusta o zoom para mostrar as unidades.
    function showunidades() {

        infoWindow.close();

        hideGroup(listadiversos);
        hideGroup(listaturismo);
        hideGroup(listaclubes);
        hideGroup(listahospedagem);
        hideGroup(listaeducacao);
        hideGroup(listajuridico);
        showGroup(listaunidades);
		
        var markers = listaunidades;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        $('#R_inicial').hide();
        $('#R_regionais').show();
        $('#R_juridico').hide();
        $('#R_lazer').hide();
        $('#R_hoteltransito').hide();
        $('#R_hotelpousada').hide();
        $('#R_educacao').hide();
        $('#R_diversos').hide();

        voltarcor();
        //cor de fundo
        $('#botaounidades').css('background-color', '#3598db');
    };

    //Ajusta o zoom para mostrar as hospedagens.
    function showhospedagens() {
        infoWindow.close();

        hideGroup(listadiversos);
        hideGroup(listaturismo);
        hideGroup(listaclubes);
        hideGroup(listaunidades);
        hideGroup(listaeducacao);
        hideGroup(listajuridico);
        showGroup(listahospedagem);

        var markers = listahospedagem;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        $('#R_inicial').hide();
        $('#R_regionais').hide();
        $('#R_juridico').hide();
        $('#R_lazer').hide();
        $('#R_hoteltransito').show();
        $('#R_hotelpousada').hide();
        $('#R_educacao').hide();
        $('#R_diversos').hide();

        voltarcor();
        //cor de fundo
        $('#botaohospedagem').css('background-color', '#2ecd71');
    };

    //Ajusta o zoom para mostrar os parceiros de educacao.
    function showeducacao() {
        infoWindow.close();

        hideGroup(listadiversos);
        hideGroup(listaturismo);
        hideGroup(listaclubes);
        hideGroup(listahospedagem);
        hideGroup(listaunidades);
        hideGroup(listajuridico);
        showGroup(listaeducacao);

        var markers = listaeducacao;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        $('#R_inicial').hide();
        $('#R_regionais').hide();
        $('#R_juridico').hide();
        $('#R_lazer').hide();
        $('#R_hoteltransito').hide();
        $('#R_hotelpousada').hide();
        $('#R_educacao').show();
        $('#R_diversos').hide();

        voltarcor();
        //cor de fundo
        $('#botaoeducacao').css('background-color', '#ab58b5');
    };

    //Ajusta o zoom para mostrar diversos.
    function showdiversos() {
        infoWindow.close();

        hideGroup(listaunidades);
        hideGroup(listaturismo);
        hideGroup(listaclubes);
        hideGroup(listahospedagem);
        hideGroup(listaeducacao);
        hideGroup(listajuridico);
        showGroup(listadiversos);

        var markers = listadiversos;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        $('#R_inicial').hide();
        $('#R_regionais').hide();
        $('#R_juridico').hide();
        $('#R_lazer').hide();
        $('#R_hoteltransito').hide();
        $('#R_hotelpousada').hide();
        $('#R_educacao').hide();
        $('#R_diversos').show();

        voltarcor();
        //cor de fundo
        $('#botaodiversos').css('background-color', '#5c6d7d');
    };

    //Ajusta o zoom para mostrar a assessoria juriidica.
    function showjuridico() {
        infoWindow.close();

        hideGroup(listadiversos);
        hideGroup(listaturismo);
        hideGroup(listaclubes);
        hideGroup(listahospedagem);
        hideGroup(listaeducacao);
        hideGroup(listaunidades);
        showGroup(listajuridico);


        var markers = listajuridico;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        $('#R_inicial').hide();
        $('#R_regionais').hide();
        $('#R_juridico').show();
        $('#R_lazer').hide();
        $('#R_hoteltransito').hide();
        $('#R_hotelpousada').hide();
        $('#R_educacao').hide();
        $('#R_diversos').hide();

        voltarcor();
        //cor de fundo
        $('#botaojuridico').css('background-color', '#e84c3d');
    };

    //Ajusta o zoom para mostrar hotel e pousada.
    function showturismo() {
        infoWindow.close();

        hideGroup(listadiversos);
        hideGroup(listaunidades);
        hideGroup(listaclubes);
        hideGroup(listahospedagem);
        hideGroup(listaeducacao);
        hideGroup(listajuridico);
        showGroup(listaturismo);

        var markers = listaturismo;
        var bounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        map.fitBounds(bounds);

        $('#R_inicial').hide();
        $('#R_regionais').hide();
        $('#R_juridico').hide();
        $('#R_lazer').hide();
        $('#R_hoteltransito').hide();
        $('#R_hotelpousada').show();
        $('#R_educacao').hide();
        $('#R_diversos').hide();

        voltarcor();
        //cor de fundo
        $('#botaohoteis').css('background-color', '#e77e23');
    };
   

    //MODAL
    $(document).ready(function () {

        $('#modal').click(function (e) {
            e.preventDefault();

            var id = $(this).attr('href');

            var maskHeight = $(document).height();
            var maskWidth = $(window).width();

            $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

            $('#mask').fadeIn(500);
            $('#mask').fadeTo("fast", 0.5);

            $(id).fadeIn(2000);

        });        

        $('.window .close').click(function (e) {
            e.preventDefault();

            $('#mask').hide();
            $('.window').hide();
        });

        $('#mask').click(function () {
            $(this).hide();
            $('.window').hide();
        });
    });

    function voltarcor() {
        $('.itemlista').css('background-color', 'rgba(79, 79, 79, 0.94)');
        
    }

