﻿jQuery(function ($) {
    $(document).ready(function () {
        $(".din").maskMoney({ thousands: '', decimal: ',', affixesStay: false, allowZero: true });        
        $('#Numero').mask("(99) 9999-9999?9");
        $('#Numero').change(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target); element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            }
            else {
                element.mask("(99) 9999-9999?9");
            }
        });

        $("#Pessoa_CPF").mask("999.999.999-99");
        $("#Pessoa_CEP").mask("99999-999");
        $("#Pessoa_NumPol").mask("999.999-9");
        $('#Pessoa_TelCel').mask("(99) 9999-9999?9");
        $('#Pessoa_TelCel').change(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target); element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            }
            else {
                element.mask("(99) 9999-9999?9");
            }
        });
        CalculaValor();
        Aparelho();
        Dados();
        //Chama a Função getSocio assim que carrega a página e a matricula não for vazia ou quando trocar de campo
        $('#Socio').change(function () {
            getSocio();
        });
        if ($("#Socio").val() != "") {
            getSocio();
        }

        $('#NumSerie').change(function () {
            getAparelho();
            UpdateValor();
        });
        if ($("#NumSerie").val() != "") {
            getAparelho();
            UpdateValor();
        }

        $(".val").change(function () {
            CalculaValor();
        })

        $(".Ap").change(function () {
            Aparelho();
        })

        $(".dad").change(function () {
            Dados();
        })

        $("#Meses").change(function () {
            CalculaTotal();
        })

        $("#add").click(function () {
            var num = $('tbody tr').size();
            var name = 'Numeros[' + num + ']';
            var numtelefone = $("#Numero").val();
            var item = '<tr id=' + num + '>' +
                       '<td>' + numtelefone + '</td>' +
                       '<input name="' + name + '" type="hidden" value="' + numtelefone + '"> ' +
                       '<td><a class="excl btn btn-danger btn-xs" >Remover</a></td></tr> ';

            if (numtelefone != "") {
                var erro = false;
                var i = 0;
                var n = numtelefone;
                for (i; i < num; i++) {
                    var a = $('#' + i + ' input').val();
                    if (a == n) {
                        erro = true;
                        alert('Número já se encontra na lista para venda')
                    }
                }
                if (erro == false) {                    
                    $('tbody').append(item);
                    CalculaValor();
                }
            }
        })

        //Remover Item
        $(document).on("click", ".excl", function () {
            var num = $('tbody tr').size();
            var id = $(this).closest('tr').attr('id');
            var val = parseInt(id) + parseInt(1);
            var par = $(this).parent().parent();
            par.remove();
            for (val; val <= num; val++) {
                var i = parseInt(val) - parseInt(1);
                $('#' + val + ' input').each(function () {
                    $(this).prop('name', 'Numeros[' + i + ']');
                })
                $('#' + val).prop('id', i);
            }
            return false;
        })
    });
});

function CalculaTotal() {
    var Meses = $("#Meses").val();
    var Mensal = $("#menv").text();
    var ap = $("#apv").text();    
    if (ap == "") ap = 0;    
    if (Meses == "") Meses = 0;
    if (Mensal == "") Mensal = 0;
    var Total = parseFloat(Meses) * parseFloat(Mensal) + parseFloat(ap);
    $("#totv").text(Total.toFixed(2));
    var Parcelas = 0;
    if (parseFloat(Meses) != 0)
        Parcelas = parseFloat(Total) / parseFloat(Meses);
    $("#parcv").text(Parcelas.toFixed(2));
    var totade = $("#adev").text();
    if (totade == "") totade = 0;
    var ValorTotal = parseFloat(Total) + parseFloat(totade);
    $("#totalv").text(ValorTotal.toFixed(2));
}

function Dados() {
    var Val = $('#PctDados').is(":checked");
    if (Val == true) {
        $("#valordad").show('slow');
        CalculaValor();
    }
    else {
        $('#valordad').hide('slow');
        $("#ValorDados").val('0.00');        
        CalculaValor();       
    }
}

//Verifica se tem Aparelho
function Aparelho() {
    var Val = $('#Sim').is(":checked");
    if (Val == true) {
        $("#NS").show('slow');
    }
    else {
        $('#Ap').hide('slow');
        $("#NS").hide('slow');
        $("#NumSerie").val('');
        $('#apv').text("0.00");
        $(".Apa dd").text('');
        UpdateValor();
    }
}

//Pega o Valor e adiciona ao resumo
function CalculaValor() {
    //Pacote de Dados
    var Dados = $("#ValorDados").val();
    if (Dados == "") Dados = "0.00";
    Dados = Dados.replace(',', '.');
    $("#pdv").text(parseFloat(Dados).toFixed(2));

    //Taxa de Adesao
    var A = $("#TaxaAdesao").is(":checked");
    var Taxa = parseFloat($("#valoradesao").val());
    if (A == true) {
        $("#adev").text(Taxa.toFixed(2))
    }
    else {
        $("#adev").text("0.00");
    }
    //Calculo dos Minutos
    var minutos = $("#Minutos").val();
    if (minutos == "") minutos = 0;
    var valminu = $("#valorminuto").val();
    var Operadora = $("#Operadora").val();    
    if (Operadora == 5) {
        minutos = $('tbody tr').size();
        $("#Minutos").val(minutos);
    }    
    var valmin = parseFloat(valminu) * parseFloat(minutos);
    
    $("#minv").text(valmin.toFixed(2))
    //Taxa de Administração
    var Adm = "0.00";
    if ($("#TaxaAdm").val() != "") { Adm = $("#TaxaAdm").val(); }

    $("#admv").text(Adm.replace(',', '.'));
    UpdateValor();
}

//Calcula o Total
function UpdateValor() {
    var totmin = $("#minv").text();
    if (totmin == "") totmin = 0;
    var totpd = $("#pdv").text();
    if (totpd == "") totpd = 0;
    /*var totade = $("#adev").text();
    if (totade == "") totade = 0;*/
    var totadm = $("#admv").text().replace(',', '.');
    if (totadm == "") totadm = 0;

    var Total = parseFloat(totmin) + parseFloat(totpd) + parseFloat(totadm);

    $("#menv").text(Total.toFixed(2));
    CalculaTotal();
}

//Função que pega os dados de acordo com a operadora
function getSocio() {
    var m = $("#Socio").val(); //Adquire valor selecionado.
    $.ajax({
        type: 'GET',
        data: { matricula: m },
        url: ('/Telefonia/getSocio/'),
        success: function (res) {
            $("#erro").text('');
            $("#Nome").text(res.Nome);
            $("#CPF").text(res.CPF);
            if (res.EmDia.length < 15) {
                $("#EmDia").attr({
                    style: 'color:green;font-weight:bold'
                });
            }
            else {
                $("#EmDia").attr({
                    style: 'color:red;font-weight:bold'
                });
            }

            $("#EmDia").text(res.EmDia);
            $("#NPol").text(res.NPol);
            $("#Endereco").text(res.Rua + ',' + res.Numero);
            $("#Bairro").text(res.Bairro);
            $("#CEP").text(res.CEP);
            $("#Municipio").text(res.Municipio);
            $("#UF").text(res.UF);
        },
        error: function (res) {
            $("#erro").text('Sócio não encontrado');
            $(".soc dd").text('');
        }
    });
}

//Busca dados do Aparelho
function getAparelho() {
    var m = $("#NumSerie").val(); //Adquire valor selecionado.
    $.ajax({
        type: 'GET',
        data: { Aparelho: m },
        url: ('/Telefonia/getAparelho/'),
        success: function (res) {
            $('#Ap').show('slow');
            $("#erroAp").text('');
            $("#Marca").text(res.Marca);
            $("#Modelo").text(res.Modelo);
            $("#op").text(res.Operadora);
            $("#Valor").text(res.Preco);
            $('#apv').text(res.Valor);
            UpdateValor();
        },
        error: function (res) {
            $("#erroAp").text('Aparelho não encontrado');
            $('#Ap').hide('slow');
            $('#apv').text('0.00');
            $(".Apa dd").text('');
            UpdateValor();
        }
    });
}

