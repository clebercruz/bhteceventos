﻿jQuery(function ($) {
    $(document).ready(function () {
        $(".val").maskMoney({ thousands: '', decimal: '.', affixesStay: false, allowZero: true });
        $('#MA').mask("9?9");
        $('#LinhaTelefonica').mask("(99) 9999-9999?9");
        $('#LinhaTelefonica').change(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target); element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            }
            else {
                element.mask("(99) 9999-9999?9");
            }
        });
        $("#TipoPag").change(function () { //Evento de modificacao do primeiro dropdownlist            
            Refpag();

        });

        $('#FormaPag').change(function () {
            FormaPag();
        });
        Refpag();
        FormaPag();   
    
    });
});

function FormaPag() {
    var TP1 = $('#TipoPag').val();
    var pag = $('#FormaPag').val();
    if ((pag == '2' || pag == '4') && TP1 != 1) {
        $('#Par').show("slow");
    }
    else if ((pag == '5' || pag == '6') && TP1 == 9) {
        $('#Par').show("slow");
    }
    else {
        $('#Par').hide("slow");
    }
}

function Refpag() {
    var TP = $('#TipoPag').val(); //Adquire valor selecionado.
    if (TP == '9') {
        $('#Outros').hide('slow');
        $('.Operadoras').hide('slow');
        $('#Ref').hide('slow');
        $('#val').show('slow');
        $.get('@Url.Action("getValorNegociacao")', { Mat: '@Model.Matricula' }, function (data) {
            if (data != "") {
                $("#Valor").val(data.valor);
                $("#Valor").attr("disabled", true);
                var item = '<input name="Valor" type="hidden" value="' + data.valor + '" />';
                $("#val").append(item);
                $("#Observacao").val(data.obs);
                var it = '<textarea class="form-control" cols="20" id="Observacao" name="Observacao" rows="2" style="resize:none" value=' + data.obs + '></textarea>';
                $("#Observacao").append(it);
            }
            else {
                alert('Sócio não possui Negociação em aberto!')
                $("#Valor").val("");
                $("#Valor").attr("disabled", true);
            }
        });
    }
    else if (TP == '1' || TP == '10' || TP == '13') {
        var Referencias = $('#RefPag');
        Referencias.empty(); //Esvazia opcoes.
        if (TP != null && TP != '') {
            if (TP == '1' || TP == '10' || TP == '13') { //Opcoes de relacionadas a veiculos
                var ident = '-- Selecione o Mês de Referência --';
                $('#Ref').show('slow'); $('#val').hide('slow'); $('#MensalidadeAdiantada').hide('slow');
                if (TP == '13') {
                    $('#MensalidadeAdiantada').show('slow');
                }
            }
            else $('#Ref').hide('slow'); $('.Operadoras').hide('slow'); $('#Outros').hide('slow');//Outras opcoes
            //Adiciona opcao padrao.
            Referencias.append($('<option/>', {
                value: '0',
                text: ident
            }));

            if (TP == '1' || TP == '10') {
                try {
                    //Adquire opcoes a partir de chamada assincrona (Veiculos ou Funcionarios).
                    $.getJSON('@Url.Action("getTipoPag")', { tipo: TP, Mat: '@Model.Matricula' }, function (refs) {
                        $.each(refs, function (index, ref) {
                            Referencias.append($('<option/>', {
                                value: ref.value,
                                text: ref.text,
                                selected: true
                            }));
                        });
                    });
                }
                catch (err) {
                    alert('Ocorreu um erro.');
                }
            }
            else {
                try {
                    //Adquire opcoes a partir de chamada assincrona (Veiculos ou Funcionarios).
                    $.getJSON('@Url.Action("getMensalidadesFuturas")', { Mat: '@Model.Matricula' }, function (refs) {
                        $.each(refs, function (index, ref) {
                            Referencias.append($('<option/>', {
                                value: ref.value,
                                text: ref.text
                            }));
                        });
                    });
                }
                catch (err) {
                    alert('Ocorreu um erro.');
                }
            }
        }
    }
    else if (TP == '8') {
        var Referencias = $('#RefPag');
        Referencias.empty();
        var ident = '-- Selecione a Reserva --';
        Referencias.append($('<option/>', {
            value: '0',
            text: ident
        }));
        try {
            //Adquire opcoes a partir de chamada assincrona (Veiculos ou Funcionarios).
            $.getJSON('@Url.Action("getReservas")', { Mat: '@Model.Matricula' }, function (refs) {
                $.each(refs, function (index, ref) {
                    Referencias.append($('<option/>', {
                        value: ref.value,
                        text: ref.text
                    }));
                });
            });
        }
        catch (err) {
            alert('Ocorreu um erro.');
        }
        $('#Ref').show('slow');
        $('#val').show('slow');
        $('#MensalidadeAdiantada').hide('slow');
    }
    else if (TP == '11') {
        $('#Outros').hide('slow');
        $('.Operadoras').show('slow');
        $('#Ref').hide('slow');
        $('#val').show('slow');
        $('#MensalidadeAdiantada').hide('slow');
    }
    else if (TP == '12') {
        $('#Outros').show('slow');
        $('.Operadoras').hide('slow');
        $('#Ref').hide('slow');
        $('#val').show('slow');
        $('#MensalidadeAdiantada').hide('slow');
    }
    else {
        $('#Outros').hide('slow');
        $('.Operadoras').hide('slow');
        $('#Ref').hide('slow');
        $('#val').show('slow');
        $('#MensalidadeAdiantada').hide('slow');
    }
}