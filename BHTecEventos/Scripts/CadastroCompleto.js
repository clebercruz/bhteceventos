﻿function proximo1() {
    var categoria = document.getElementById("Categoria").value;
    if (categoria != '') {
        document.getElementById('tela1').style.display = 'none';
        document.getElementById('tela2').style.display = 'block';
    }
}
function proximo2() {
    document.getElementById('tela2').style.display = 'none';
    var categoria = document.getElementById("Categoria").value;
    if (categoria == 1 || categoria == 2 || categoria == 3 || categoria == 4) {
        document.getElementById('tela3').style.display = 'block';
    }
    else {
        document.getElementById('tela4').style.display = 'block';
    }
}
function proximo3() {
    document.getElementById('tela4').style.display = 'block';
    document.getElementById('tela3').style.display = 'none';
}
function voltar1() {
    document.getElementById('tela1').style.display = 'block';
    document.getElementById('tela2').style.display = 'none';
}
function voltar2() {
    document.getElementById('tela2').style.display = 'block';
    document.getElementById('tela3').style.display = 'none';
}
function voltar3() {
    document.getElementById('tela4').style.display = 'none';
    var categoria = document.getElementById("Categoria").value;
    if (categoria == 1 || categoria == 2 || categoria == 3 || categoria == 4) {
        document.getElementById('tela3').style.display = 'block';
    }
    else {
        document.getElementById('tela2').style.display = 'block';
    }
}