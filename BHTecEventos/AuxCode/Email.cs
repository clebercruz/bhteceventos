﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace BHTecEventos.AuxCode
{
    public class Email
    {

        String Destinatario { get; set; }
        String Conteudo { get; set; }
        String Assunto { get; set; }


        public Email(String destinatario, String assunto, String conteudo)
        {
            this.Assunto = assunto; this.Destinatario = destinatario; this.Conteudo = conteudo;
        }


        public void Envia()
        {
            //Cria objeto responsável pela mensagem de email.
            MailMessage objEmail = new MailMessage();

            //Remtente do email.
            objEmail.From = new MailAddress("aspraemail@site1380748432.provisorio.ws");

            //email para resposta(quando o destinatário receber e clicar em responder, vai para:)
            objEmail.ReplyToList.Add(new MailAddress("cosmenavarro@gmail.com"));
            objEmail.ReplyToList.Add(new MailAddress("renato.miranda.assis@gmail.com"));

            //Destinatário do email, pode ser mais de um.
            objEmail.To.Add(this.Destinatario);

            //Prioridade do email.
            objEmail.Priority = MailPriority.Normal;

            //Ativar conteúdo html?
            objEmail.IsBodyHtml = false;

            //Assunto do email.
            objEmail.Subject = this.Assunto;

            //Corpo do email.
            objEmail.Body = this.Conteudo;

            //Codificação do assunto.
            objEmail.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");

            //Codificação do corpo.
            objEmail.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");

            //Anexar arquivos
            //objEmail.Attachments.Add(new Attachment("C:/Users/Lab2/Desktop/Teste.txt"));

            //Cria o objeto que envia o email.
            SmtpClient objSmtp = new SmtpClient();

            //Endereço do servidor SMTP.
            //objSmtp.Host = "smtp.site1380748432.provisorio.ws";
            objSmtp.Host = "email-ssl.com.br";

            objSmtp.EnableSsl = true;

            objSmtp.Port = 587;

            //Login e Senha do email.
            objSmtp.Credentials = new NetworkCredential("aspraemail@site1380748432.provisorio.ws", "h$131517");

            //envia o email.
            objSmtp.Send(objEmail);
        }


    }
}