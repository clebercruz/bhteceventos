﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace BHTecEventos.AuxCode
{
    public class Sec
    {
        //Chave de encriptação.
        static byte[] bytes = ASCIIEncoding.ASCII.GetBytes("myf4rmI2");

        public static string Encrypt(string originalString)
        {
            if (String.IsNullOrEmpty(originalString))
            {
                throw new ArgumentNullException("The string which needs to be encrypted can not be null.");
            }

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(bytes, bytes),
                CryptoStreamMode.Write);
            var writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        public static string Decrypt(string encryptedString)
        {
            if (String.IsNullOrEmpty(encryptedString))
            {
                throw new ArgumentNullException("The string which needs to be decrypted can not be null.");
            }

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream(Convert.FromBase64String(encryptedString));
            var cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(bytes, bytes),
                CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }

        public static bool IsValidAdmin(String CPFusuario, String senha)
        {
            bool isValid = false;

            using (var db = new BHTecEventos.Models.EntitiesBHTecEventos())
            {
                var user = db.Usuarios.FirstOrDefault(u => u.CPF == CPFusuario.ToUpper());

                if (user != null)
                {
                    
                    var oi = Decrypt(user.Senha);
                    if (user.Senha == Encrypt(senha))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }

        public static bool IsActive(String CPFUsr, String senha)
        {
            bool isActive = false;
            using (var db = new BHTecEventos.Models.EntitiesBHTecEventos())
            {
                var user = db.Usuarios.FirstOrDefault(u => u.CPF == CPFUsr);

                if (user != null)
                {
                    if (user.Senha == Encrypt(senha) && user.Ativo)
                    {
                        isActive = true;
                    }
                }
                
            }
            return isActive;
        }
    }
}