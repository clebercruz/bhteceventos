﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using BHTecEventos.Models;

namespace BHTecEventos.AuxCode
{
    public class TT
    {

        public static bool VerificaPermissao(String User, int idPermissao)
        {
            if (User == "Administrador") return true;
            using (var db = new EntitiesBHTecEventos())
            {
                var Usuario = db.Usuarios.FirstOrDefault(u => u.Nome == User);
                return (Usuario != null) ? (db.PermissaoUsuarios.FirstOrDefault(u => u.IdUsuario== Usuario.Codigo && u.IdPermissao == idPermissao) != null) : false;
            }
        }

        public static String SemAcentoMaiuscula(String str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                str = TT.SemAcento(str).ToUpper();
            }
            return str;
        }

        public static string SemAcento(string palavra)
        {
            string palavraSemAcento = null;
            string caracterComAcento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
            string caracterSemAcento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";

            for (int i = 0; i < palavra.Length; i++)
            {
                if (caracterComAcento.IndexOf(Convert.ToChar(palavra.Substring(i, 1))) >= 0)
                {
                    int car = caracterComAcento.IndexOf(Convert.ToChar(palavra.Substring(i, 1)));
                    palavraSemAcento += caracterSemAcento.Substring(car, 1);
                }
                else
                {
                    palavraSemAcento += palavra.Substring(i, 1);
                }
            }
            return palavraSemAcento;
        }

        public static String getEstadoCivil(int i)
        {
            EntitiesBHTecEventos db = new EntitiesBHTecEventos();
            var est = db.EstadoCivil.Find(i);
            if(est !=null)
            {
                return est.NomeEstadoCivil;
            }
            else
            {
                return  "-----";
            }
        }

    }
}