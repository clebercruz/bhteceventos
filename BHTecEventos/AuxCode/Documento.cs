﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using BHTecEventos.Models;
using System.Globalization;

namespace BHTecEventos.AuxCode
{
    public class Documento
    {
        public static void geraComprovanteBHTecEventos(Pessoas_Eventos pessoa)
        {
            //Este método recebe uma pessoa e gera o comprovante de particiáção no Seminário.

            var pdfPath = Path.Combine(HttpContext.Current.Server.MapPath("~/ModelosDocumentos/Certificado.pdf"));

            String DtImpressao = System.DateTime.Now.ToString("MMMM");

            var formFieldMap = PDFHelper.GetFormFieldNames(pdfPath);
            formFieldMap["NomeParticipante"] = pessoa.Pessoas.Nome;
            formFieldMap["NomeEvento"] = pessoa.Eventos.Nome;
            formFieldMap["CargaHoraria"] = pessoa.Eventos.CargaHoraria.ToString();
            formFieldMap["DataEventoDia"] = pessoa.Eventos.DataAcontecimento.ToString("dd");
            formFieldMap["DataEventoMes"] = pessoa.Eventos.DataAcontecimento.ToString("MM");
            formFieldMap["DataEventoAno"] = pessoa.Eventos.DataAcontecimento.Year.ToString();
            formFieldMap["DataImpressaoDia"] = System.DateTime.Now.ToString("dd");
            formFieldMap["DataImpressaoMes"] = DtImpressao[0].ToString().ToUpper() + DtImpressao.Substring(1);
            formFieldMap["DataImpressaoAno"] = System.DateTime.Now.Year.ToString();
            var pdfContents = PDFHelper.GeneratePDF(pdfPath, formFieldMap);

            PDFHelper.ReturnPDF(pdfContents, "Certificado_" + pessoa.Pessoas.Nome.Replace(" ","") + ".pdf");
        }
    }
}