﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BHTecEventos.Models
{
    public class EventoModel
    {
        public int Id { get; set; }

        [Display(Name = "Nome:")]
        public string Nome { get; set; }

        [Display(Name = "Data Criação:")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataCriado { get; set; }

        [Display(Name = "Data Limite:")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public string DataLimite { get; set; }

        [Display(Name = "Data do Evento:")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public string DataAcontecimento { get; set; }

        [Display(Name = "Limite de Pessoas:")]
        public int LimitePessoas { get; set; }

        [Display(Name = "Carga Horária:")]
        public int CargaHoraria { get; set; }

        [Display(Name = "Palestrante:")]
        public string Organizador { get; set; }

        [Display(Name = "Responsável:")]
        public int UsuarioResponsavel { get; set; }

        public string NomeUsuario { get; set; }

        [Display(Name = "Status:")]
        public bool Ativo { get; set; }




    }

    public class CadastroEventoModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage="Informe o nome do evento")]
        public string Nome { get; set; }
      
        public DateTime DataCriado { get; set; }
        [Required(ErrorMessage = "Informe a data limite para inscrição do evento")]
        public DateTime DataLimite { get; set; }
        [Required(ErrorMessage = "Informe o limite de pessoas no evento")]
        public int LimitePessoas { get; set; }
        public int CargaHoraria { get; set; }
        public string Organizador { get; set; }        
        public int UsuarioResponsavel { get; set; }
        public string NomeUsuario { get; set; }        
        public bool Ativo { get; set; }
        [Required(ErrorMessage = "Informe a data do evento")]
        public DateTime DataAcontecimento { get; set; }
        public SelectList ListaUsuarios { get; set; }
    }


    public class BuscaCadastradosModel
    {
        public String Nome { get; set; }
        public String Evento { get; set; }
        public String DataInicio { get; set; }
        public String DataFim { get; set; }
    }

    public partial class ExibePessoaModel
    {
        public Pessoas Pessoa { get; set; }
        public Eventos Evento { get; set; }
        public List<Pessoas_Eventos> QntEvento { get; set; }
    }


    public partial class RelEventoModel
    {
        public string NomeEvento { get; set; }
        public int TotalIncritos { get; set; }
        public int TotalPresente { get; set; }
        public List<PostoEventoModel> Postos { get; set; }
    }

    public partial class PostoEventoModel
    {
        public string Descricao { get; set; }
        public int Quant { get; set; }
        public int QuantPresentes { get; set; }


        public int? Cod { get; set; }
    }

    public class EventoCPFModel
    {

        [Required(ErrorMessage = "Favor preencher o seu CPF")]
        [Display(Name = "CPF")]
        public string CPF { get; set; }
    }

}