﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BHTecEventos.Models
{
    public class EmailUnicoAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var email = value.ToString();

            var db = new BHTecEventos.Models.EntitiesBHTecEventos();
            //var usuario = db.Usuarios.FirstOrDefault(u => u.Email == email);

            //return usuario == null;
            return false;
        }
    }

    public class ValidaCPFAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;
            var cpf = value.ToString().Replace(".", "").Replace("-", "");

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
                if (cpf[i] != cpf[0])
                    igual = false;

            if (igual || cpf == "12345678909")
                return false;

            int[] numeros = new int[11];

            for (int i = 0; i < 11; i++)
                numeros[i] = int.Parse(cpf[i].ToString());

            int soma = 0;
            for (int i = 0; i < 9; i++)
                soma += (10 - i) * numeros[i];

            int resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                    return false;
            }
            else if (numeros[9] != 11 - resultado)
                return false;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += (11 - i) * numeros[i];

            resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                    return false;
            }
            else
                if (numeros[10] != 11 - resultado)
                    return false;

            return true;
        }
    }


    public class CustomAuthorize : AuthorizeAttribute
    {
        private int[] nivel;
        public CustomAuthorize(int[] nivel)
        {
            this.nivel = nivel;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                bool Authorize = true;
                var userIdentity = httpContext.User.Identity;

                if (!userIdentity.IsAuthenticated)
                    return !Authorize;

                //if (userIdentity.Name == "admin")
                //    return Authorize;
                //foreach (var item in nivel)
                //{
                //    if (TT.VerificaPermissao(userIdentity.Name, item))
                //        return Authorize;
                //}
                return !Authorize;
            }
            //Provavelmente socio tentando acessar admin.
            catch (NullReferenceException)
            {
                return false;
            }
        }
    }
}