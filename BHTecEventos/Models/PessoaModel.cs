﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BHTecEventos.Models;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace BHTecEventos.Models
{
    public class PessoaModel
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Favor preencha o seu Nome")]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor preencher o seu CPF")]
        [Display(Name = "CPF")]
        public string CPF { get; set; }


        [Required(ErrorMessage = "Favor preencher a Rua")]
        [Display(Name = "Rua")]
        public string Rua { get; set; }

        [Required(ErrorMessage = "Favor preencher o Numero")]
        [Display(Name = "Número")]
        public string Numero { get; set; }


        [Display(Name = "Complemento")]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "Favor preencher o CEP")]
        [Display(Name = "CEP")]
        public string CEP { get; set; }

        [Required(ErrorMessage = "Favor preencher o Bairro")]
        [Display(Name = "Bairro")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Favor preencher o Municipio")]
        [Display(Name = "Município")]
        public string Municipio { get; set; }
        public SelectList ListaMunicipios { get; set; }

        [Required(ErrorMessage = "Favor preencher a UF")]
        [Display(Name = "Estado")]
        public string UF { get; set; }

        [Display(Name = "RG")]
        public string RG { get; set; }


        [Required(ErrorMessage = "Favor preencher a Data de Nascimento")]
        [Display(Name = "Data de Nascimento")]
        public Nullable<System.DateTime> DataNasc { get; set; }

        [Required(ErrorMessage = "Favor preencher o Estado Civil")]
        [Display(Name = "Estado Civil")]
        public Nullable<int> EstadoCivil { get; set; }
        public SelectList ListaEstadoCivil { get; set; }

        [Required(ErrorMessage = "Favor preencher o Sexo")]
        [Display(Name = "Sexo")]
        public string Sexo { get; set; }

        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Display(Name = "Telefone Celular")]
        public string TelCel { get; set; }

        [Display(Name = "Telefone Residencial")]
        public string TelRes { get; set; }

        [Display(Name = "Profissão")]
        public string Profissao { get; set; }
        public DateTime DataInscricao { get; set; }

        public SelectList ListaEventos { get; set; }

        public string CadastrarPessoa(PessoaModel PM)
        {
            string Erro = "";
            try
            {

                using (var db = new EntitiesBHTecEventos())
                {
                    Pessoas P = new Pessoas();

                    P.Nome = PM.Nome;
                    P.CPF = PM.CPF;
                    P.Rua = PM.Rua;
                    P.Numero = PM.Numero;
                    P.ComplementoEndereco = PM.Complemento;
                    P.CEP = PM.CEP;
                    P.Bairro = PM.Bairro;
                    P.Municipio = PM.Municipio;
                    P.UF = PM.UF;
                    P.RG = PM.RG;
                    P.DataNascimento = PM.DataNasc;
                    P.EstadoCivil = PM.EstadoCivil;
                    P.Sexo = PM.Sexo;
                    P.Email = PM.Email;
                    P.TelCel = PM.TelCel;
                    P.TelRes = PM.TelRes;
                    P.Profissao = PM.Profissao;
                    P.DataInscricao = DataInscricao = System.DateTime.Now;
                    db.Pessoas.Add(P);
                    db.SaveChanges();
                }

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    //Erro += "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:"+ eve.Entry.Entity.GetType().Name+ eve.Entry.State;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",ve.PropertyName, ve.ErrorMessage);
                        //Erro += "- Property: \"{0}\", Error: \"{1}\"" + ve.PropertyName + ve.ErrorMessage;
                        Erro += ve.ErrorMessage + "\n";
                    }
                }

            }

            return Erro;
        }

        public virtual ICollection<Pessoas_Eventos> Pessoas_Eventos { get; set; }

        public PessoaModel BuscarPessoa(Pessoas PM)
        {
            
            using (var db = new EntitiesBHTecEventos())
            {
                Id = PM.Id;
                Nome = PM.Nome;
                CPF = PM.CPF;
                Rua = PM.Rua;
                Numero = PM.Numero;
                Complemento = PM.ComplementoEndereco;
                CEP = PM.CEP;
                Bairro = PM.Bairro;
                Municipio = PM.Municipio;
                UF = PM.UF;
                RG = PM.RG;
                DataNasc = PM.DataNascimento;
                EstadoCivil = PM.EstadoCivil;
                Sexo = PM.Sexo;
                Email = PM.Email;
                TelCel = PM.TelCel;
                TelRes = PM.TelRes;
                Profissao = PM.Profissao;
                DataInscricao = System.DateTime.Now;
                Pessoas_Eventos = PM.Pessoas_Eventos;
            }

            return this;

        }

        public string AtualizarPessoa(PessoaModel PM)
        {
            string Erro = "";
            try
            {

                using (var db = new EntitiesBHTecEventos())
                {
                    Pessoas P = db.Pessoas.FirstOrDefault(i => i.CPF == PM.CPF);


                    P.Nome = PM.Nome;
                    P.CPF = PM.CPF;
                    P.Rua = PM.Rua;
                    P.Numero = PM.Numero;
                    P.ComplementoEndereco = PM.Complemento;
                    P.CEP = PM.CEP;
                    P.Bairro = PM.Bairro;
                    P.Municipio = PM.Municipio;
                    P.UF = PM.UF;
                    P.RG = PM.RG;
                    P.DataNascimento = PM.DataNasc;
                    P.EstadoCivil = PM.EstadoCivil;
                    P.Sexo = PM.Sexo;
                    P.Email = PM.Email;
                    P.TelCel = PM.TelCel;
                    P.TelRes = PM.TelRes;
                    P.Profissao = PM.Profissao;
                    P.DataInscricao = System.DateTime.Now;
                    db.Entry(P).State = EntityState.Modified;
                    db.SaveChanges();
                }


            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    //Erro += "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:" + eve.Entry.Entity.GetType().Name + eve.Entry.State;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        //Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",ve.PropertyName, ve.ErrorMessage);
                        //Erro += "- Property: \"{0}\", Error: \"{1}\"" + ve.PropertyName + ve.ErrorMessage;
                        Erro += ve.ErrorMessage + "\n";
                    }
                }

            }

            return Erro;
        }


    }


    public class ListaPessoasModel
    {
        public int Id { get; set; }
        public int Matricula { get; set; }
        public string Nome { get; set; }

        [Required(ErrorMessage = "Favor preencher o seu CPF")]
        [Display(Name = "CPF")]
        public string CPF { get; set; }
        public string TelRes { get; set; }
        public string Rua { get; set; }
        public string Numero { get; set; }
        public string CEP { get; set; }
        public string Bairro { get; set; }
        public string Municipio { get; set; }
        public string UF { get; set; }
        public string RG { get; set; }
        public string NumPol { get; set; }
        public string Complemento { get; set; }
        public Nullable<int> SocioId { get; set; }
        public Nullable<System.DateTime> DataNasc { get; set; }
        public Nullable<int> EstadoCivil { get; set; }
        public string Sexo { get; set; }
        public string Email { get; set; }
        public string TelCel { get; set; }
        public int Status { get; set; }
    }

}